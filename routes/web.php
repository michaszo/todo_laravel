<?php

Route::view('/', 'welcome');

Route::get('todos', 'TodosController@index')
    ->name('todos');

Route::get('todos/{todo}', 'TodosController@show');

Route::get('new_todo', 'TodosController@create')
    ->name('new_todo');

Route::post('store_todo', 'TodosController@store')
    ->name('store_todo');

Route::get('todos/{todo}/delete', 'TodosController@destroy');

Route::get('todos/{todo}/edit', 'TodosController@edit');

Route::post('todos/{todo}/update', 'TodosController@update');

Route::post('todos/{todo}/completed', 'TodosController@completed');
